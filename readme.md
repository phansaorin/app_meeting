## MyCEO

LCRM is a laravel based hosted CRM.

## install
````composer install````

````cp .env.example .env````

````php artisan key:generate````

open .env and enter database details

````php artisan migrate````

````php artisan db:seed````

````set chmod 777 in public/uploads folder and it's sub folders````

now you can access the website