<?php

return [

    "categories" => "Categories",
    "new" => "New category",
    "edit" => "Edit category",
    "delete" => "Delete category",
    "name" => "Category name",
    "details" => "Category details"
];
