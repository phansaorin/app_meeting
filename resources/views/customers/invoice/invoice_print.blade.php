<html>
<head>
    <style>
        body {
            font-family: "Open Sans", Arial, sans-serif;
            font-size: 14px;
            line-height: 22px;
            margin: 0;
            padding: 0;
        }

        table {
            background-color: transparent;
            border-collapse: collapse;
            border-spacing: 0;
            max-width: 100%;
        }

        .main {
            width: 1024px;
            margin: 0px auto;
        }

        .main_detail {
            width: 100%;
            margin: 10px auto;
            float: left;
        }

        .head_item_fl {
            width: 100%;
            float: left;
            margin-bottom: 30px;
            margin-top: -100px !important;
            border-bottom: 1px solid #555;
            padding-bottom: 10px;
        }

        .logo_item {
            width: 50%;
            float: left
        }

        .lt_item {
            width: 50%;
            float: left;
            text-align: right;
            font-size: 18px;
            height: 68px;
            line-height: 68px;
        }

        .detail_view_item {
            float: left;
            height: auto;
            margin-bottom: 20px;
            width: 100%;
        }

        .view_title_bg td {
            background: #7fa637 none repeat scroll 0 0;
            color: #fff;
            font-weight: 700;
        }

        .view_frist {
            border: 0px !important;
            width: 50%;
            float: left;
            padding-left: 0px !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
            line-height: 24px;
        }

        .view_second {
            border: 0px !important;
            padding-left: 0 !important;
        }

        .detail_view_item td {
            color: #656565;
            padding: 4px 10px;
        }

        .detail_view_item table tr td {
            border: 1px solid #d6d6d5;
            font-size: 14px;
        }

        .view_bg_one {
            background: #f3f3f3;
        }

        .detail_head_titel {
            /*background: #f3f3f3;*/
            padding: 5px 5px 5px 0px;
            width: 100%;
            font-size: 30px;
            height: 44px;
            line-height: 30px;
            box-sizing: border-box;
            margin-bottom: 20px;
            float: left;
        }
    </style>
</head>
<body>
<div class="main" style="margin-top:-90px !important">
    <div class="main_detail">
        <div class="detail_view_item">
            <div class="view_frist">
                <b>{{trans('invoice.shipping_address')}}:</b><br/>
                {{isset($customer)?$customer->address:""}}
            </div>
            <div class="view_frist">
                {{isset($customer)?$customer->address:""}}
            </div>
        </div>
        <div class="detail_head_titel">{{trans('invoice.invoice_no')}} {{$invoice->invoice_number}}</div>
        <div class="detail_view_item">
            <div style="width:250px; float:left;">
                <span><b>{{trans('invoice.customer')}}
                        :</b><br>{{ is_null($invoice->customer)?"":$invoice->customer->full_name }}</span>
            </div>
            <div style="width:250px; float:left;">
                <span><b>{{trans('invoice.invoice_date')}}:</b><br>{{ $invoice->invoice_date}}</span>
            </div>
            <div style="width:250px; float:left;">
                <span><b>{{trans('invoice.due_date')}}:</b><br>{{ $invoice->due_date}}</span>
            </div>
            <div style="width:250px; float:left;">
                <span><b>{{trans('invoice.payment_term')}}
                        :</b><br>{{ $invoice->payment_term.' '.trans('invoice.days') }}</span>
            </div>
            <div style="width:250px; float:left;">
                <span><b>{{trans('invoice.sales_team_id')}}
                        :</b><br>{{ is_null($invoice->salesTeam)?"":$invoice->salesTeam->salesteam }}</span>
            </div>
            <div style="width:250px; float:left;">
                <span><b>{{trans('invoice.salesperson_id')}}
                        :</b><br> {{ is_null($invoice->salesPerson)?"":$invoice->salesPerson->full_name }}</span>
            </div>
        </div>
        <div class="detail_view_item">
            {{trans('invoice.products')}}
            <table width="100%" cellspacing="0" cellpadding="0" border="">
                <tbody>
                <tr>
                    <td><b>{{trans('invoice.product')}}</b></td>
                    <td><b>{{trans('invoice.quantity')}}</b></td>
                    <td><b>{{trans('invoice.unit_price')}}</b></td>
                    <td><b>{{trans('invoice.taxes')}}</b></td>
                    <td><b>{{trans('invoice.subtotal')}}</b></td>
                </tr>
                @foreach ($invoice->products as $qo_product)
                    <tr>
                        <td>{{$qo_product->product_name}}</td>
                        <td>{{ $qo_product->quantity}}</td>
                        <td>{{ $qo_product->price}}</td>
                        <td>{{ number_format($qo_product->quantity * $qo_product->price * Settings::get('sales_tax') / 100, 2,
                        '.', '') }}
                        </td>
                        <td>{{ $qo_product->sub_total }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="detail_view_item" style="float:right; text-align:right; width:400px;">
            <table width="100%" cellspacing="0" cellpadding="0" border="" class="pull-right">
                <tbody>
                <tr>
                    <td style="width:50%;"><b>{{trans('invoice.untaxed_amount')}}</b></td>
                    <td>{{ $invoice->total }}</td>
                </tr>
                <tr>
                    <td>{{trans('invoice.taxes')}}</td>
                    <td>{{ $invoice->tax_amount }}</td>
                </tr>
                <tr>
                    <td><b>{{trans('invoice.total')}}</b></td>
                    <td>{{ $invoice->grand_total }}</td>
                </tr>
                <tr>
                    <td>{{trans('invoice.discount')}}</td>
                    <td>{{ $invoice->discount }}</td>
                </tr>
                <tr>
                    <td><b>{{trans('invoice.final_price')}}</b></td>
                    <td>{{ $invoice->final_price }}</td>
                </tr>
                <tr>
                    <td><b>{{trans('invoice.unpaid_amount')}}</b></td>
                    <td>{{ $invoice->unpaid_amount }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>