@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-3" id="mail-menu">
            <a href="#" class="btn btn-info btn-sm btn-block" data-toggle="modal" id="create_email"
               data-target="#modal-create_email">{{trans('mailbox.compose')}}
            </a>
            <div class="panel">
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="#inbox_emails" data-toggle="tab" data-toggle="pill"
                               class="btn btn-fw btn-sm">
                                <i class="fa fa-inbox fa-fw mrs"></i>
                                {{trans('mailbox.inbox')}}
                            </a>
                        </li>
                        <li>
                            <a href="#sent_emails" data-toggle="tab" data-toggle="pill"
                               class="btn btn-fw btn-sm">
                                <i class="fa fa-plane fa-fw mrs"></i>
                                {{trans('mailbox.send_mail')}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-lg-9 tab-content table-bordered">
            <div class="tab-pane fade in active" id="inbox_emails">
                <div class="mail-box-header">
                    <h2>{{trans('mailbox.inbox')}} ({{$email_list->count()}})</h2>
                </div>
                <div class="mail-box">
                    <div class="table-responsive">
                        <table class="table" id="inbox-check">
                            <thead>
                            <tr>
                                <td>{{trans('mailbox.sender')}}</td>
                                <td>{{trans('mailbox.subject')}}</td>
                                <td>{{trans('mailbox.message')}}</td>
                                <td>{{trans('mailbox.created')}}</td>
                                <td>{{trans('mailbox.read')}}</td>
                                <td class="text-right">{{trans('mailbox.delete')}}</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if( !empty($email_list) )
                                @foreach( $email_list as $list)
                                    <tr>
                                        <td class="view-message hidden-xs">
                                            @if(isset($list->sender->id))
                                                @if(isset($list->sender->user_avatar))
                                                    <img src="{{ url('uploads/avatar/thumb_'.$list->sender->user_avatar) }}"
                                                         width="27" height="27"
                                                         class="img-circle img-responsive pull-left" alt="Image">
                                                @else
                                                    <img src="{{ url('uploads/avatar/user.png') }}"
                                                         width="27" height="27"
                                                         class="img-circle img-responsive pull-left" alt="Image">
                                                @endif
                                                {{$list->sender->first_name}} {{$list->sender->last_name}}
                                            @endif
                                        </td>
                                        <td class="view-message ">
                                            {{$list->subject}}
                                        </td>
                                        <td class="view-message inbox-small-cells">
                                            {{ $list->message}}
                                        </td>
                                        <td class="view-message">
                                            {{$list->created_at->diffForHumans()}}
                                        </td>
                                        <td class="view-message">
                                            @if($list->read==0)
                                                <a href="#">
                                                    <i class="fa fa-fw fa-square-o text-warning read" id="{{$list->id}}"></i>
                                                </a>
                                            @else
                                                <i class="fa fa-fw  fa-check-square-o text-success"></i>
                                            @endif
                                        </td>
                                        <td class="view-message text-right">
                                            <a href="#">
                                                <i class="fa fa-fw fa-times text-danger delete" id="{{$list->id}}"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="sent_emails">
                <div class="mail-box-header">
                    <h2>{{trans('mailbox.send_mail')}} ({{$sent_email_list->count()}})</h2>
                </div>
                <div class="mail-box">
                    <div class="table-responsive">
                        <table class="table" id="inbox-check">
                            <thead>
                            <tr>
                                <td>{{trans('mailbox.receiver')}}</td>
                                <td>{{trans('mailbox.subject')}}</td>
                                <td>{{trans('mailbox.message')}}</td>
                                <td>{{trans('mailbox.created')}}</td>
                                <td class="text-right">{{trans('mailbox.delete')}}</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if( !empty($sent_email_list) )
                                @foreach( $sent_email_list as $list)
                                    <tr>
                                        <td class="view-message hidden-xs">
                                            @if(isset($list->receiver->id))
                                                @if(isset($list->receiver->user_avatar))
                                                    <img src="{{ url('uploads/avatar/thumb_'.$list->receiver->user_avatar) }}"
                                                         width="27" height="27"
                                                         class="img-circle img-responsive pull-left" alt="Image">
                                                @else
                                                    <img src="{{ url('uploads/avatar/user.png') }}"
                                                         width="27" height="27"
                                                         class="img-circle img-responsive pull-left" alt="Image">
                                                @endif
                                                {{$list->receiver->first_name}} {{$list->receiver->last_name}}
                                            @endif
                                        </td>
                                        <td class="view-message ">
                                            {{$list->subject}}
                                        </td>
                                        <td class="view-message inbox-small-cells">
                                            {{ $list->message}}
                                        </td>
                                        <td class="view-message">
                                            {{$list->created_at->diffForHumans()}}
                                        </td>
                                        <td class="view-message text-right">
                                            <a href="#">
                                                <i class="fa fa-fw fa-times text-danger delete" id="{{$list->id}}"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-create_email" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times-circle-o"></i>
                        </button>
                        <h4 class="modal-title">
                            <strong>{{trans('mailbox.write')}}</strong> {{trans('mailbox.an_email')}}
                        </h4>
                    </div>
                    <div id="send_email_ajax">

                    </div>
                    {!! Form::open(array('method' => 'post','files'=> true, 'name'=>"send_email")) !!}
                    <div class="modal-body">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="form-group">
                                    {!! Form::label('to_email_id', trans('mailbox.to_email_id'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::select('to_email_id[]', $staffs, null, array('id'=>'to_email_id','class' => 'form-control select2', 'data-placeholder'=>trans('mailbox.select_receiver'))) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('subject', trans('mailbox.subject'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('subject', null, array('id'=>'subject','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('message', trans('mailbox.message'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::textarea('message', null, array('id'=>'message','class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer text-center">
                            <button type="submit" id="send" class="btn btn-primary">
                                {{trans('mailbox.send')}}
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            $("form[name='send_email']").unbind('submit').bind('submit', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var formData = new FormData($(this)[0]);
                $('#send').prop('disabled', false);
                $.ajax({
                    url: "{{url('customers/mailbox/send_email')}}",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (msg) {
                        $('body,html').animate({scrollTop: 0}, 200);
                        $("#send_email_ajax").html(msg);
                        $("form[name='send_email']").find("input[type=text], textarea").val("");
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        var msg = '';
                        $.each(errors, function (index, value) {
                            msg += '<div class="alert alert-danger">' + value + '</div>';
                        });
                        $("#send_email_ajax").html(msg);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            });
        });
        function close_email() {
            $(".email-details h1").empty();
            $(".email-details .sender").empty();
            $(".email-details .date").empty();
            $(".email-details .email-content").empty();

            $("#email_details_show").hide();
        }

        function open_email() {
            $("#email_details_show").show();
        }

        $('.delete').on('click', function () {
            var id = this.id;
            $.ajax({
                type: "GET",
                url: "{{url('customers/mailbox')}}/" + id + "/delete_email",
                success: function () {
                    $('#' + id).parent().parent().parent().fadeOut('normal');

                }
            });
        })
        $('.read').on('click', function () {
            var id = this.id;
            $.ajax({
                type: "GET",
                url: "{{url('customers/mailbox')}}/" + id + "/read_email",
                success: function () {
                    $('#' + id).parent().html('<i class="fa fa-fw  fa-check-square-o text-success"></i>');

                }
            });
        })
    </script>
@stop