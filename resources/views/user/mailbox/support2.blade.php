@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                <div class="row-body scrollable hover">
                    <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" id="create_email"
                       data-target="#modal-create_email">{{trans('mailbox.compose')}}
                    </a>
                    <a href="#inbox_emails" data-toggle="tab"
                       class="btn btn-info btn-sm">{{trans('mailbox.inbox')}}</a>
                    <a href="#sent_emails" data-toggle="tab"
                       class="btn btn-success btn-sm">{{trans('mailbox.send_support')}}</a>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="inbox_emails">
                            <div class="messages-list withScroll show-scroll" data-height="window">
                                <strong>{{trans('mailbox.inbox')}}</strong>
                                @if( !empty($email_list) )
                                    <ul class="list-group">
                                        @foreach( $email_list as $list)
                                            <li class="list-group-item">
                                                <div class="message-item media" id="email_id_{{$list->id}} ">
                                                    <div class="media-left col-sm-1">
                                                        @if(isset($sent_list->sender->user_avatar))
                                                            <img src="{{ url('uploads/avatar/thumb_'.$sent_list->sender->user_avatar) }}"
                                                                 alt="user image" width="40" class="sender-img">
                                                        @else
                                                            <img src="{{ url('uploads/avatar/user.png') }}"
                                                                 alt="user image" width="40" class="sender-img"/>
                                                        @endif
                                                        <div>{{$list->sender->first_name}} {{$list->sender->last_name}}</div>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="media-heading">{{$list->subject}}</h5>

                                                        <div><strong>{{ $list->message}}</strong></div>
                                                        <p><a id="{{$list->sender->id}}" href="#"
                                                              class="btn btn-primary btn-sm send-replay"
                                                              data-toggle="modal"
                                                              data-target="#modal-create_replay">{{trans('mailbox.replay')}}
                                                            </a></p>

                                                        <div>{{date('m/d/Y g:i a', $list->date_time)}}</div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="sent_emails">
                            <div class="messages-list withScroll show-scroll" data-height="window">
                                <strong>{{trans('mailbox.send_support')}}</strong>
                                @if( !empty($sent_email_list) )
                                    <ul class="list-group">
                                        @foreach( $sent_email_list as $list)
                                            <li class="list-group-item">
                                                <div class="message-item media" id="email_id_{{$list->id}} ">
                                                    <div class="media-left col-sm-1">
                                                        @if(isset($sent_list->receiver->user_avatar))
                                                            <img src="{{ url('uploads/avatar/thumb_'.$sent_list->receiver->user_avatar) }}"
                                                                 alt="user image" width="40" class="sender-img">
                                                        @else
                                                            <img src="{{ url('uploads/avatar/user.png') }}"
                                                                 alt="user image" width="40" class="sender-img"/>
                                                        @endif
                                                        <div>{{$list->receiver->first_name}} {{$list->receiver->last_name}}</div>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="media-heading">{{$list->subject}}</h5>

                                                        <div><strong>{{ $list->message}}</strong></div>
                                                        <div>{{date('m/d/Y g:i a', $list->date_time)}}</div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="list-group no-radius no-borders light lt">
                <div>{{trans('mailbox.online')}}</div>
                @foreach($users_list as $item)
                    @if($item['active']=='1')
                        <a class="list-group-item text-ellipsis" data-dismiss="modal" data-target="#chat"
                           data-toggle="modal">
                            @if(isset($item['user_avatar']))
                                <img class="img-circle"
                                     src="{{ url('uploads/avatar/thumb_'.$item['user_avatar']) }}"
                                     alt="user image" width="40" class="sender-img img-responsive">
                            @else
                                <img class="img-circle" src="{{ url('uploads/avatar/user.png') }}"
                                     alt="user image" width="40" class="sender-img img-responsive"/>
                            @endif
                            <i class="fa fa-circle text-success text-xs"></i>
                            <span>{{$item['full_name']}}</span>
                        </a>
                    @endif
                @endforeach
                <div>{{trans('mailbox.offline')}}</div>
                @foreach($users_list as $item)
                    @if($item['active']=='0')
                        <a class="list-group-item text-ellipsis" data-dismiss="modal" data-target="#chat"
                           data-toggle="modal">
                            @if(isset($item['user_avatar']))
                                <img class="img-circle"
                                     src="{{ url('uploads/avatar/thumb_'.$item['user_avatar']) }}"
                                     alt="user image" width="40" class="sender-img img-responsive">
                            @else
                                <img class="img-circle" src="{{ url('uploads/avatar/user.png') }}"
                                     alt="user image" width="40" class="sender-img img-responsive"/>
                            @endif
                            <i class="fa fa-circle text-warning text-xs"></i>
                            <span>{{$item['full_name']}}</span>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-create_email" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle-o"></i>
                    </button>
                    <h4 class="modal-title"><strong>{{trans('mailbox.write')}}</strong> {{trans('mailbox.an_support')}}
                    </h4>
                </div>
                <div id="send_email_ajax">

                </div>
                {!! Form::open(array('method' => 'post','files'=> true, 'name'=>"send_support")) !!}
                <div class="modal-body">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('etemplate_id', trans('mailbox.etemplate_id'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::select('etemplate_id', $email_templates, null, array('id'=>'etemplate_id','class' => 'form-control select2')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('to', trans('mailbox.to_email_id'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::select('to[]', $users, null, array('id'=>'to','multiple','class' => 'form-control select2')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('subject', trans('mailbox.subject'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::text('subject', null, array('id'=>'subject','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('message', trans('mailbox.message'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::textarea('message', null, array('id'=>'message','class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="send_email_submitbutton" class="modal-footer text-center">
                        <button type="submit" class="btn btn-primary btn-embossed bnt-square">
                            {{trans('mailbox.send')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-create_replay" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle-o"></i>
                    </button>
                    <h4 class="modal-title"><strong>{{trans('mailbox.write')}}</strong> {{trans('mailbox.an_support')}}
                    </h4>
                </div>
                <div id="send_email_ajax">

                </div>
                {!! Form::open(array('method' => 'post','files'=> true, 'name'=>"send_support_replay")) !!}
                <div class="modal-body">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('etemplate_id2', trans('mailbox.etemplate_id'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::select('etemplate_id', $email_templates, null, array('id'=>'etemplate_id2','class' => 'form-control select2')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::hidden('to', null, array('class' => 'control-label', 'id'=>"replay_to")) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('subject', trans('mailbox.subject'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::text('subject', null, array('id'=>'subject','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('message2', trans('mailbox.message'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::textarea('message', null, array('id'=>'message2','class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="send_email_submitbutton" class="modal-footer text-center">
                        <button type="submit" class="btn btn-primary btn-embossed bnt-square">
                            {{trans('mailbox.send')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.warning').click(function () {
                $("#replay_to").val($(this).attr('id'));
            });
            $("form[name='send_support']").submit(function (e) {
                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: "{{url('support/send_support')}}",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (msg) {
                        $('body,html').animate({scrollTop: 0}, 200);
                        $("#send_email_ajax").html(msg);
                        $("#send_email_submitbutton").html('<button type="submit" class="btn btn-primary btn-embossed bnt-square">{{trans('mailbox.send')}}</button>');

                        $("form[name='send_support']").find("input[type=text], textarea").val("");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                e.preventDefault();
            });
            $("form[name='send_support_replay']").submit(function (e) {
                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: "{{url('support/send_support_replay')}}",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (msg) {
                        $('body,html').animate({scrollTop: 0}, 200);
                        $("#send_email_ajax").html(msg);
                        $("#send_email_submitbutton").html('<button type="submit" class="btn btn-primary btn-embossed bnt-square">{{trans('mailbox.send')}}</button>');

                        $("form[name='send_support']").find("input[type=text], textarea").val("");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                e.preventDefault();
            });
            $('#create_email').click(function () {
                $('#etemplate_id').change(function () {
                    if ($(this).val() > 0) {
                        $.ajax({
                            type: "GET",
                            url: '{{url("email_template/ajax_get_template")}}/' + $(this).val(),
                            success: function (data) {
                                $("#message").html(data.text);
                            },
                        });
                    }
                });
            });
            $('.send-replay').click(function () {
                $('#etemplate_id2').change(function () {
                    if ($(this).val() > 0) {
                        $.ajax({
                            type: "GET",
                            url: '{{url("email_template/ajax_get_template")}}/' + $(this).val(),
                            success: function (data) {
                                $("#message2").html(data.text);
                            },
                        });
                    }
                });
            });


        });

    </script>
@stop