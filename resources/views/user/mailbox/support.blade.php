@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="row-body scrollable hover">
            <a href="#" class="btn btn-primary btn-sm" data-toggle="modal"
               data-target="#modal-create_email">{{trans('mailbox.compose')}}
            </a>
            <a href="#inbox_emails" data-toggle="tab"
               class="btn btn-info btn-sm">{{trans('mailbox.inbox')}}</a>
            <a href="#sent_emails" data-toggle="tab"
               class="btn btn-success btn-sm">{{trans('mailbox.send_support')}}</a>
        </div>
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="inbox_emails">
                    <div class="messages-list withScroll show-scroll" data-height="window">
                        <strong>{{trans('mailbox.inbox')}}</strong>
                        @if( !empty($email_list) )
                            <ul class="list-group">
                                @foreach( $email_list as $list)
                                    <li class="list-group-item">
                                        <div class="message-item media" id="email_id_{{$list->id}} ">
                                            <div class="media-left col-sm-1">
                                                @if(isset($sent_list->sender->user_avatar))
                                                    <img src="{{ url('uploads/avatar/thumb_'.$sent_list->sender->user_avatar) }}"
                                                         alt="user image" width="40" class="sender-img img-responsive">
                                                @else
                                                    <img src="{{ url('uploads/avatar/user.png') }}"
                                                         alt="user image" width="40" class="sender-img img-responsive"/>
                                                @endif
                                                <div>{{$list->sender->first_name}} {{$list->sender->last_name}}</div>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">{{$list->subject}}</h5>

                                                <div><strong>{{ $list->message}}</strong></div>
                                                <div>{{$item->created_at->format('d.m.Y. H:i')}}</div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="tab-pane fade in" id="sent_emails">
                    <div class="messages-list withScroll show-scroll" data-height="window">
                        <strong>{{trans('mailbox.send_support')}}</strong>
                        @if( !empty($sent_email_list) )
                            <ul class="list-group">
                                @foreach( $sent_email_list as $list)
                                    <li class="list-group-item">
                                        <div class="message-item media" id="email_id_{{$list->id}} ">
                                            <div class="media-left col-sm-1">
                                                @if(isset($sent_list->receiver->user_avatar))
                                                    <img src="{{ url('uploads/avatar/thumb_'.$sent_list->receiver->user_avatar) }}"
                                                         alt="user image" width="40" class="sender-img img-responsive">
                                                @else
                                                    <img src="{{ url('uploads/avatar/user.png') }}"
                                                         alt="user image" width="40" class="sender-img img-responsive"/>
                                                @endif
                                                <div>{{$list->receiver->first_name}} {{$list->receiver->last_name}}</div>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">{{$list->subject}}</h5>

                                                <div><strong>{{ $list->message}}</strong></div>
                                                <div>{{date('m/d/Y g:i a', $list->date_time)}}</div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-create_email" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times-circle-o"></i>
                    </button>
                    <h4 class="modal-title"><strong>{{trans('mailbox.write')}}</strong> {{trans('mailbox.an_support')}}
                    </h4>
                </div>
                <div id="send_email_ajax">

                </div>
                {!! Form::open(array('method' => 'post','files'=> true, 'name'=>"send_support")) !!}
                <div class="modal-body">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('subject', trans('mailbox.subject'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::text('subject', null, array('id'=>'subject','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('message', trans('mailbox.message'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    {!! Form::textarea('message', null, array('id'=>'message','class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="send_email_submitbutton" class="modal-footer text-center">
                        <button type="submit" class="btn btn-primary btn-embossed bnt-square">
                            {{trans('mailbox.send')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            $("form[name='send_support']").submit(function (e) {
                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: "{{url('support/send_support')}}",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (msg) {
                        $('body,html').animate({scrollTop: 0}, 200);
                        $("#send_email_ajax").html(msg);
                        $("#send_email_submitbutton").html('<button type="submit" class="btn btn-primary btn-embossed bnt-square">{{trans('mailbox.send')}}</button>');

                        $("form[name='send_support']").find("input[type=text], textarea").val("");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                e.preventDefault();
            });
        });
    </script>
@stop