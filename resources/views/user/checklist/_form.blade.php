<div class="panel panel-primary">
    <div class="panel-body">
        <!-- Start of company infomation -->
        @if (isset($checklist))
        {!! Form::model($checklist, array('url' => $type . '/' . $checklist->id, 'method' => 'put', 'files'=> true)) !!}
        @else
        {!! Form::open(array('url' => $type, 'method' => 'post', 'files'=> true)) !!}
        @endif
        <div>
            <h3>Company Information</h3>
                <div class="form-group required">
                {!! Form::label('template_title', trans('checklist.template_title'), array('class' => 'control-label required')) !!}
                <div class="controls">
                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
                </div>
                </div>
                <div class="form-group required {{ $errors->has('bussines_nature') ? 'has-error' : '' }}">
                    <?php 
                        $bussines_natures = [
                            '1' => 'IT',
                            '2' => 'Development',
                            '3' => 'ISP'
                        ];
                    ?>
                    {!! Form::label('bussines_nature', trans('checklist.bussines_nature'), array('class' => 'control-label required')) !!}
                    <div class="controls">
                        {!! Form::select('bussines_nature', $bussines_natures, (isset($product)?$product->bussines_nature:null), array('id'=>'bussines_nature','class' => 'form-control')) !!}
                        <span class="help-block">{{ $errors->first('bussines_nature', ':message') }}</span>
                    </div>
                </div>
                <div class="form-group required">
                    {!! Form::label('description', trans('checklist.description'), array('class' => 'control-label')) !!}
                    <div class="controls">
                    {!! Form::textarea('description', null, array('class' => 'form-control')) !!}
​
                    </div>
                </div>
                <!-- The end of company information -->
                <div class="form-group">
                
            </div> <!-- form group-->
        </div>
​
        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                <a href="" class="btn btn-primary">
                    <i class="fa fa-arrow-left"></i> {{trans('checklist.concel')}}
                </a>
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-check-square-o"></i> {{trans('checklist.save')}}
                </button>
            </div>
        </div>
        <!-- ./ form actions --> 
        {!! Form::close() !!}
    </div>
</div>
@section('scripts')
​
<script>
$(document).ready(function () {
    $('#AddSession').hide();
    var AddButton = $('#AddMoreFileBox');
    $(AddButton).click(function(){
        $('#AddSession').show();
    });
    // Sub adding
    $('.choice_radio').hide();
    $('.btn-choice').hide();
    $('select#selected_value').on('change', function() {
        var selectVal = $("#selected_value option:selected").val();
        var selectTxt = $("#selected_value option:selected").text();
        if (selectVal == 1) {
            $('.choice_radio').show();
            $('.btn-choice').show();
        }else if(selectVal == 3){
            $('.choice_radio').hide();
            $('.btn-choice').hide();
        };
    });
});
</script>
@endsection