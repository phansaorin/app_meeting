@extends('layouts.user')

{{-- Web site Title --}}
@section('title')
    {{ $title }}
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/icheck.css') }}" type="text/css">
@stop
{{-- Content --}}
@section('content')
    <div class="panel panel-primary" xmlns:v-bind="http://symfony.com/schema/routing">
        <div class="panel-body">
            <span class="pull-right">
                <a href="#" class="text-muted">
                    <i class="fa fa-gear"></i>
                </a>
            </span>
            {!! Form::open(array('url' => '/setting', 'method' => 'post', 'files'=> true)) !!}

            <div class="nav-tabs-custom" id="setting_tabs">
                <ul class="nav nav-tabs" style="display:list-item;">
                    <li class="active">
                        <a href="#general_configuration"
                           data-toggle="tab" title="{{ trans('settings.general_configuration') }}"><i class="material-icons md-24">build</i></a>
                    </li>
                    <li>
                        <a href="#payment_configuration"
                           data-toggle="tab" title="{{ trans('settings.payment_configuration') }}"><i class="material-icons md-24">attach_money</i></a>
                    </li>
                    <li>
                        <a href="#start_number_prefix_configuration"
                           data-toggle="tab" title="{{ trans('settings.start_number_prefix_configuration') }}"><i class="material-icons md-24">settings_applications</i></a>
                    </li>
                    <li>
                        <a href="#pusher_configuration"
                           data-toggle="tab" title="{{ trans('settings.pusher_configuration') }}"><i class="material-icons md-24">receipt</i></a>
                    </li>
                    <li>
                        <a href="#paypal_settings"
                           data-toggle="tab" title="{{ trans('settings.paypal_settings') }}"><i class="material-icons md-24">payment</i></a>
                    </li>
                    <li>
                        <a href="#stripe_settings"
                           data-toggle="tab" title="{{ trans('settings.stripe_settings') }}"><i class="material-icons md-24">vpn_key</i></a>
                    </li>
                    <li>
                        <a href="#available_modules"
                           data-toggle="tab" title="{{ trans('settings.available_modules') }}"><i class="material-icons md-24">widgets</i></a>
                    </li>
                    <li>
                        <a href="#backup_configuration"
                           data-toggle="tab" title="{{ trans('settings.backup_configuration') }}"><i class="material-icons md-24">backup</i></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="general_configuration">
                        <div class="form-group required {{ $errors->has('site_logo') ? 'has-error' : '' }} ">
                            {!! Form::label('site_logo_file', trans('settings.site_logo'), array('class' => 'control-label')) !!}
                            <div class="controls row" v-image-preview>
                                <img src="{{ url('uploads/site/'.Settings::get('site_logo')) }}"
                                     class="img-l col-sm-2">
                                {!! Form::file('site_logo_file', null, array('id'=>'site_logo', 'class' => 'form-control')) !!}
                                <img id="image-preview" width="300">
                                <span class="help-block">{{ $errors->first('site_logo', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('site_name') ? 'has-error' : '' }}">
                            {!! Form::label('site_name', trans('settings.site_name'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('site_name', old('site_name', Settings::get('site_name')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('site_name', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('site_email') ? 'has-error' : '' }}">
                            {!! Form::label('site_email', trans('settings.site_email'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('site_email', old('site_email', Settings::get('site_email')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('site_email', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('allowed_extensions') ? 'has-error' : '' }}">
                            {!! Form::label('allowed_extensions', trans('settings.allowed_extensions'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('allowed_extensions', old('allowed_extensions', Settings::get('allowed_extensions')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('allowed_extensions', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('max_upload_file_size') ? 'has-error' : '' }}">
                            {!! Form::label('max_upload_file_size', trans('settings.max_upload_file_size'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::select('max_upload_file_size', $max_upload_file_size, old('max_upload_file_size', Settings::get('max_upload_file_size')), array('id'=>'max_upload_file_size','class' => 'form-control select2')) !!}
                                <span class="help-block">{{ $errors->first('max_upload_file_size', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="payment_configuration">
                        <div class="form-group required {{ $errors->has('sales_tax') ? 'has-error' : '' }}">
                            {!! Form::label('sales_tax', trans('settings.sales_tax').'%', array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','sales_tax', old('sales_tax', Settings::get('sales_tax')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('sales_tax', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('payment_term1') ? 'has-error' : '' }}">
                            {!! Form::label('payment_term1', trans('settings.payment_term1'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','payment_term1', old('payment_term1', Settings::get('payment_term1')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('payment_term1', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('payment_term2') ? 'has-error' : '' }}">
                            {!! Form::label('payment_term2', trans('settings.payment_term2'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','payment_term2', old('payment_term2', Settings::get('payment_term2')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('payment_term2', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('payment_term3') ? 'has-error' : '' }}">
                            {!! Form::label('payment_term3', trans('settings.payment_term3'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','payment_term3', old('payment_term3', Settings::get('payment_term3')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('payment_term3', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('opportunities_reminder_days') ? 'has-error' : '' }}">
                            {!! Form::label('opportunities_reminder_days', trans('settings.opportunities_reminder_days'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','opportunities_reminder_days', old('opportunities_reminder_days', Settings::get('opportunities_reminder_days')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('opportunities_reminder_days', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('contract_renewal_days') ? 'has-error' : '' }}">
                            {!! Form::label('contract_renewal_days', trans('settings.contract_renewal_days'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','contract_renewal_days', old('contract_renewal_days', Settings::get('contract_renewal_days')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('contract_renewal_days', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('invoice_reminder_days') ? 'has-error' : '' }}">
                            {!! Form::label('invoice_reminder_days', trans('settings.invoice_reminder_days'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','invoice_reminder_days', old('invoice_reminder_days', Settings::get('invoice_reminder_days')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('invoice_reminder_days', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('currency') ? 'has-error' : '' }}">
                            {!! Form::label('currency', trans('settings.currency'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::select('currency', $options['currency'], old('currency', Settings::get('currency')), array('id'=>'currency','class' => 'form-control select2')) !!}
                                <span class="help-block">{{ $errors->first('currency', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="start_number_prefix_configuration">
                        <div class="form-group required {{ $errors->has('quotation_prefix') ? 'has-error' : '' }}">
                            {!! Form::label('quotation_prefix', trans('settings.quotation_prefix'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('quotation_prefix', old('quotation_prefix', Settings::get('quotation_prefix')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('quotation_prefix', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('quotation_start_number') ? 'has-error' : '' }}">
                            {!! Form::label('quotation_start_number', trans('settings.quotation_start_number'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','quotation_start_number', old('quotation_start_number', Settings::get('quotation_start_number')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('quotation_start_number', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('sales_prefix') ? 'has-error' : '' }}">
                            {!! Form::label('sales_prefix', trans('settings.sales_prefix'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('sales_prefix', old('sales_prefix', Settings::get('sales_prefix')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('sales_prefix', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('sales_start_number') ? 'has-error' : '' }}">
                            {!! Form::label('sales_start_number', trans('settings.sales_start_number'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','sales_start_number', old('sales_start_number', Settings::get('sales_start_number')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('sales_start_number', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('invoice_prefix') ? 'has-error' : '' }}">
                            {!! Form::label('invoice_prefix', trans('settings.invoice_prefix'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('invoice_prefix', old('invoice_prefix', Settings::get('invoice_prefix')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('invoice_prefix', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('invoice_start_number') ? 'has-error' : '' }}">
                            {!! Form::label('invoice_start_number', trans('settings.invoice_start_number'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','invoice_start_number', old('invoice_start_number', Settings::get('invoice_start_number')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('invoice_start_number', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('invoice_payment_prefix') ? 'has-error' : '' }}">
                            {!! Form::label('invoice_payment_prefix', trans('settings.invoice_payment_prefix'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('invoice_payment_prefix', old('invoice_payment_prefix', Settings::get('invoice_payment_prefix')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('invoice_payment_prefix', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('invoice_payment_start_number') ? 'has-error' : '' }}">
                            {!! Form::label('invoice_payment_start_number', trans('settings.invoice_payment_start_number'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('number','invoice_payment_start_number', old('invoice_payment_start_number', Settings::get('invoice_payment_start_number')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('invoice_payment_start_number', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pusher_configuration">
                        <div class="form-group required {{ $errors->has('pusher_app_id') ? 'has-error' : '' }}">
                            {!! Form::label('pusher_app_id', trans('settings.pusher_app_id'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','pusher_app_id', old('pusher_app_id', Settings::get('pusher_app_id')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('pusher_app_id', ':message') }}</span>
                            </div>
                        </div>

                        <div class="form-group required {{ $errors->has('pusher_key') ? 'has-error' : '' }}">
                            {!! Form::label('pusher_key', trans('settings.pusher_key'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','pusher_key', old('pusher_key', Settings::get('pusher_key')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('pusher_key', ':message') }}</span>
                            </div>
                        </div>

                        <div class="form-group required {{ $errors->has('pusher_secret') ? 'has-error' : '' }}">
                            {!! Form::label('pusher_secret', trans('settings.pusher_secret'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','pusher_secret', old('pusher_secret', Settings::get('pusher_secret')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('pusher_secret', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="paypal_settings">
                        <div class="form-group required {{ $errors->has('paypal_testmode') ? 'has-error' : '' }}">
                            {!! Form::label('paypal_testmode', trans('settings.paypal_testmode'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                <div class="form-inline">
                                    <div class="radio">
                                        {!! Form::radio('paypal_testmode', 'true',(Settings::get('paypal_testmode')=='true')?true:false,array('class' => 'icheck'))  !!}
                                        {!! Form::label('true', 'True')  !!}
                                    </div>
                                    <div class="radio">
                                        {!! Form::radio('paypal_testmode', 'false', (Settings::get('paypal_testmode')=='false')?true:false,array('class' => 'icheck'))  !!}
                                        {!! Form::label('false', 'False') !!}
                                    </div>
                                </div>
                                <span class="help-block">{{ $errors->first('paypal_testmode', ':message') }}</span>
                            </div>
                        </div>

                        <div class="form-group required {{ $errors->has('paypal_username') ? 'has-error' : '' }}">
                            {!! Form::label('paypal_username', trans('settings.paypal_username'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','paypal_username', old('paypal_username', Settings::get('paypal_username')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('paypal_username', ':message') }}</span>
                            </div>
                        </div>

                        <div class="form-group required {{ $errors->has('paypal_password') ? 'has-error' : '' }}">
                            {!! Form::label('paypal_password', trans('settings.paypal_password'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','paypal_password', old('paypal_password', Settings::get('paypal_password')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('paypal_password', ':message') }}</span>
                            </div>
                        </div>

                        <div class="form-group required {{ $errors->has('paypal_signature') ? 'has-error' : '' }}">
                            {!! Form::label('paypal_signature', trans('settings.paypal_signature'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','paypal_signature', old('paypal_signature', Settings::get('paypal_signature')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('paypal_signature', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="stripe_settings">
                        <div class="form-group required {{ $errors->has('stripe_secret') ? 'has-error' : '' }}">
                            {!! Form::label('stripe_secret', trans('settings.stripe_publishable'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','stripe_secret', old('stripe_secret', Settings::get('stripe_secret')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('stripe_secret', ':message') }}</span>
                            </div>
                        </div>
                        <div class="form-group required {{ $errors->has('stripe_publishable') ? 'has-error' : '' }}">
                            {!! Form::label('stripe_publishable', trans('settings.stripe_secret'), array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::input('text','stripe_publishable', old('stripe_publishable', Settings::get('stripe_publishable')), array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('stripe_publishable', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="available_modules">
                        <div class="form-group">
                            <legend>{{trans('settings.available_modules')}}</legend>
                            <label>
                                <input type="checkbox" value="contracts" name="modules[]"
                                       @if(in_array('contracts', Settings::get('modules'))) checked
                                       @endif class="icheck"> {{trans('settings.contracts')}}
                            </label>
                        </div>
                    </div>


                    <div class="tab-pane" id="backup_configuration">
                        <backup-settings backup_type="{{ Settings::get('backup_type') }}"
                                         :options="{{ $opts['backup_type'] }}" inline-template>
                            <div class="form-group required {{ $errors->has('backup_type') ? 'has-error' : '' }}">
                                {!! Form::label('backup_type', trans('settings.backup_type'), array('class' => 'control-label')) !!}
                                <div class="controls">
                                    <select v-model="backup_type" name="backup_type" class="form-control">
                                        <option v-for="option in options" v-bind:value="option.id">
                                            @{{ option.text }}
                                        </option>
                                    </select>
                                    <span class="help-block">{{ $errors->first('backup_type', ':message') }}</span>
                                </div>
                            </div>

                            {{-- Dropbox --}}
                            <div v-if="backup_type=='dropbox'">
                                <div class="form-group required {{ $errors->has('disk_dbox_key') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_dbox_key', trans('settings.disk_dbox_key'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_dbox_key', old('disk_dbox_key', Settings::get('disk_dbox_key')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('disk_dbox_key', ':message') }}</span>
                                    </div>
                                </div>


                                <div class="form-group required {{ $errors->has('disk_dbox_secret') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_dbox_secret', trans('settings.disk_dbox_secret'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_dbox_secret', old('disk_dbox_secret', Settings::get('disk_dbox_secret')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('disk_dbox_secret', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="form-group required {{ $errors->has('disk_dbox_token') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_dbox_token', trans('settings.disk_dbox_token'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_dbox_token', old('disk_dbox_token', Settings::get('disk_dbox_token')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('disk_dbox_token', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="form-group required {{ $errors->has('disk_dbox_app') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_dbox_app', trans('settings.disk_dbox_app'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_dbox_app', old('disk_dbox_app', Settings::get('disk_dbox_app')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('disk_dbox_app', ':message') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div v-if="backup_type=='s3'">
                                {{-- AWS S3 --}}
                                <div class="form-group required {{ $errors->has('disk_aws_key') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_aws_key', trans('settings.disk_aws_key'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_aws_key', old('disk_aws_key', Settings::get('disk_aws_key')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('disk_aws_key', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="form-group required {{ $errors->has('disk_aws_secret') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_aws_secret', trans('settings.disk_aws_secret'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_aws_secret', old('disk_aws_secret', Settings::get('disk_aws_secret')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('disk_aws_secret', ':message') }}</span>
                                    </div>
                                </div>


                                <div class="form-group required {{ $errors->has('disk_aws_bucket') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_aws_bucket', trans('settings.disk_aws_bucket'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_aws_bucket', old('disk_aws_bucket', Settings::get('disk_aws_bucket')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('site_nbucket', ':message') }}</span>
                                    </div>
                                </div>


                                <div class="form-group required {{ $errors->has('disk_aws_region') ? 'has-error' : '' }}">
                                    {!! Form::label('disk_aws_region', trans('settings.disk_aws_region'), array('class' => 'control-label')) !!}
                                    <div class="controls">
                                        {!! Form::text('disk_aws_region', old('disk_aws_region', Settings::get('disk_aws_region')), array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('site_nregion', ':message') }}</span>
                                    </div>
                                </div>
                            </div>
                        </backup-settings>
                    </div>

                </div>
            </div>
            <!-- Form Actions -->
            <div class="form-group">
                <div class="controls">
                    <button type="submit" class="btn btn-success"><i
                                class="fa fa-check-square-o"></i> {{trans('table.ok')}}</button>
                </div>
            </div>
            <!-- ./ form actions -->

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/icheck.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.icheck').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
@stop