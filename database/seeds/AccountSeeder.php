<?php

use App\Models\Customer;

class AccountSeeder extends DatabaseSeeder
{

    public function run()
    {

        $admin = Sentinel::registerAndActivate(array(
            'email' => 'admin@crm.com',
            'password' => "admin",
            'first_name' => 'Admin',
            'last_name' => 'Doe',
        ));
        $admin->user_id = $admin->id;
        $admin->save();

        $staff = Sentinel::registerAndActivate(array(
            'email' => 'staff@crm.com',
            'password' => "staff",
            'first_name' => 'Staff',
            'last_name' => 'Doe',
        ));
        $admin->users()->save($staff);

        foreach ($this->getPermissions() as $permission) {
            $staff->addPermission($permission);
        }
        $staff->save();

        $customer = Sentinel::registerAndActivate(array(
            'email' => 'customer@crm.com',
            'password' => "customer",
            'first_name' => 'Customer',
            'last_name' => 'Doe',
        ));
        Customer::create(array('user_id' => $customer->id, 'belong_user_id' => $staff->id));
        $staff->users()->save($customer);

        $adminRole = Sentinel::getRoleRepository()->createModel()->create(array(
            'name' => 'Admin',
            'slug' => 'admin',
        ));

        $staffRole = Sentinel::getRoleRepository()->createModel()->create(array(
            'name' => 'Staff',
            'slug' => 'staff',
        ));

        $customerRole = Sentinel::getRoleRepository()->createModel()->create(array(
            'name' => 'Customer',
            'slug' => 'customer',
        ));

        $adminRole->users()->attach($admin);
        $staffRole->users()->attach($staff);
        $customerRole->users()->attach($customer);

    }


    private function getPermissions()
    {
        return [
            'sales_team.read',
            'sales_team.write',
            'sales_team.delete',
            'leads.read',
            'leads.write',
            'leads.delete',
            'opportunities.read',
            'opportunities.write',
            'opportunities.delete',
            'logged_calls.read',
            'logged_calls.write',
            'logged_calls.delete',
            'meetings.read',
            'meetings.write',
            'meetings.delete',
            'products.read',
            'products.write',
            'products.delete',
            'quotations.read',
            'quotations.write',
            'quotations.delete',
            'sales_orders.read',
            'sales_orders.write',
            'sales_orders.delete',
            'invoices.read',
            'invoices.write',
            'invoices.delete',
            'customers.read',
            'customers.write',
            'customers.delete',
            'contracts.read',
            'contracts.write',
            'contracts.delete',
            'staff.read',
            'staff.write',
            'staff.delete',
        ];
    }

}