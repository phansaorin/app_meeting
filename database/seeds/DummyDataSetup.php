<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DummyDataSetup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App::environment() === 'local') {
            DB::table('sales_teams')->truncate();
            DB::table('opportunities')->truncate();

            //Delete existing seeded users except the first 4 users
            User::where('id', '>', 3)->get()->each(function ($user) {
                $user->forceDelete();
            });

            //Get the default ADMIN
            $user = User::find(1);
            $staffRole = Sentinel::getRoleRepository()->findByName('staff');
            $customerRole = Sentinel::getRoleRepository()->findByName('customer');

            //Seed Sales teams for default ADMIN
            foreach (range(1, 4) as $j) {
                $this->createSalesTeam($user->id, $j, $user);
                $this->createOpportunity($user, $user->id, $j);
            }

            //Get the default STAFF
            $staff = User::find(2);
            $this->createSalesTeam($staff->id, 1, $staff);
            $this->createSalesTeam($staff->id, 2, $staff);

            //Seed Sales teams for each STAFF
            foreach (range(1, 4) as $j) {
                $this->createSalesTeam($staff->id, $j, $staff);
                $this->createOpportunity($staff, $staff->id, $j);
            }

            foreach (range(1, 3) as $i) {
                $staff = $this->createStaff($i);
                $user->users()->save($staff);
                $staffRole->users()->attach($staff);

                $customer = $this->createCustomer($i);
                $staff->users()->save($customer);
                $customerRole->users()->attach($customer);
                $customer->customer()->save(factory(\App\Models\Customer::class)->make());


                //Seed Sales teams for each STAFF
                foreach (range(1, 5) as $j) {
                    $this->createSalesTeam($staff->id, $j, $staff);
                    $this->createOpportunity($staff, $i, $j);
                }

            }
        }
    }


    /**
     * @param $i
     * @param $j
     * @param $staff
     */
    private function createSalesTeam($i, $j, $staff)
    {
        $salesTeam = factory(\App\Models\Salesteam::class)->make([
            'salesteam' => 'STeam - ' . $i . ' - ' . $j,
            'team_leader' => $staff->id
        ]);
        return $staff->salesteams()->save($salesTeam);
    }

    /**
     * @param $i
     * @return mixed
     */
    private function createStaff($i)
    {
        $staff = Sentinel::registerAndActivate(array(
            'email' => 'staff' . $i . '@crm.com',
            'password' => "staff",
            'first_name' => 'Staff',
            'last_name' => $this->convertNumberToWord($i),
        ));
        return $staff;
    }

    /**
     * @param $staff
     * @param $i
     * @param $j
     */
    private function createOpportunity($staff, $i, $j)
    {
        $opprtunity = $staff->opportunities()->save(factory(\App\Models\Opportunity::class)->make([
            'opportunity' => 'Opp ' . $i . ' - ' . $j,
            'stages' => array_rand($this->stages())
        ]));

        return $opprtunity;
    }

    private function stages()
    {
        return [
            'New' => 'New',
            'Qualification' => 'Qualification',
            'Proposition' => 'Proposition',
            'Negotiation' => 'Negotiation',
            'Won' => 'Won',
            'Lost' => 'Lost',
            'Dead' => 'Dead',
        ];
    }

    private function convertNumberToWord($num = false)
    {
        $num = str_replace(array(',', ' '), '', trim($num));
        if (!$num) {
            return false;
        }
        $num = (int)$num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int)(($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int)($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ($hundreds == 1 ? '' : 's') . ' ' : '');
            $tens = (int)($num_levels[$i] % 100);
            $singles = '';
            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
            } else {
                $tens = (int)($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int)($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . (($levels && ( int )($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }

    /**
     * @return mixed
     */
    private function createCustomer($i)
    {
        //$customer = factory(User::class)->make();
        $customer = Sentinel::registerAndActivate(array(
            'email' => 'customer' . $i . '@crm.com',
            'password' => "customer",
            'first_name' => 'Customer',
            'last_name' => $this->convertNumberToWord($i),
        ));

        return $customer;
    }
}
