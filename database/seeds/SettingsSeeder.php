<?php


use Efriandika\LaravelSettings\Facades\Settings;

class SettingsSeeder extends DatabaseSeeder
{

    public function run()
    {
        Settings::set('site_name', "LCRM");
        Settings::set('site_logo', 'logo.png');
        Settings::set('site_email', 'info@domain.com');
        Settings::set('allowed_extensions', 'gif,jpg,jpeg,png,pdf,txt');
        Settings::set('max_upload_file_size', 2000);
        Settings::set('sales_tax', '16');
        Settings::set('payment_term1', 5);
        Settings::set('payment_term2', 10);
        Settings::set('payment_term3', 15);
        Settings::set('opportunities_reminder_days', 5);
        Settings::set('contract_renewal_days', 7);
        Settings::set('invoice_reminder_days', 7);
        Settings::set('quotation_prefix', 'Q000');
        Settings::set('quotation_start_number', 0);
        Settings::set('sales_prefix', 'S000');
        Settings::set('sales_start_number', 0);
        Settings::set('invoice_prefix', 'I000');
        Settings::set('invoice_start_number', 0);
        Settings::set('invoice_payment_prefix', 'P000');
        Settings::set('invoice_payment_start_number', 0);
        Settings::set('backup_type', 'local');
        Settings::set('modules', []);

    }

}