<?php

namespace App\Http\Controllers\Customer;

use App\Events\Email\EmailCreated;
use App\Http\Controllers\UserController;
use App\Http\Requests\MailboxRequest;
use App\Http\Requests\SupportRequest;
use App\Models\Company;
use App\Models\Email;
use App\Models\EmailTemplate;
use App\Models\User;
use App\Repositories\CompanyRepository;
use App\Repositories\ContractRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Efriandika\LaravelSettings\Facades\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Sentinel;
use App\Http\Requests;

class MailboxController extends UserController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;

        view()->share('type', 'mailbox');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::where('main_contact_person', $this->user->id)->get();
        $company_ids = array();
        foreach ($companies as $company)
        {
            $company_ids[] = $company->id;
        }
        $email_list = Email::whereIn('assign_customer_id', $company_ids)->where('delete_receiver','0')->orderBy('id','desc')->get();
        $sent_email_list = Email::where('from', $this->user->id)->where('delete_sender','0')->orderBy('id','desc')->get();
        $title = trans('mailbox.mailbox');

        $staffs = $this->userRepository->getAllForCustomer()
            ->get()->filter(function ($user) {
                return ($user->inRole('staff')||$user->inRole('admin'));
            })
            ->lists('full_name', 'id')->toArray();

        return view('customers.mailbox.index', compact('title', 'email_list', 'sent_email_list', 'staffs'));
    }

    function sendEmail(MailboxRequest $request)
    {
        $message_return = '<div class="alert alert-danger">' . trans('mailbox.danger') . '</div>';
        if (isset($request->to_email_id)) {
            $email = new Email($request->except('to_email_id'));
            $email->to = $request->to_email_id;
            $email->from = Sentinel::getUser()->id;
            $email->save();

            event(new EmailCreated($email));

            $user = User::find($request->to_email_id);

            if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
                Mail::send('emails.contact', array('user' => $user->first_name . ' ' . $user->last_name, 'bodyMessage' => $request->message),
                function ($m)
                use ($user, $request) {
                    $m->from(Settings::get('site_email'), Settings::get('site_name'));
                    $m->to($user->email)->subject($request->subject);
                });
            }

            $message_return = '<div class="alert alert-success">' . trans('mailbox.success') . '</div>';

        }
        echo $message_return;

    }

    function deleteMail(Email $mail)
    {
        $customer = $this->user->customer;
        if($mail->assign_customer_id == $customer->company_id){
            $mail->delete_receiver= 1;
        }
        else{
            $mail->delete_sender= 1;
        }
        $mail->save();
    }

    function readMail(Email $mail)
    {
        $mail->read = 1;
        $mail->save();
    }

}
