<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\UserController;
use App\Http\Requests\EmailTemplateRequest;
use App\Models\EmailTemplate;
use App\Http\Requests;
use App\Repositories\EmailTemplateRepository;
use Datatables;
use Sentinel;

class EmailTemplateController extends UserController
{
    /**
     * @var EmailTemplateRepository
     */
    private $emailTemplateRepository;

    /**
     * EmailTemplateController constructor.
     * @param EmailTemplateRepository $emailTemplateRepository
     */
    public function __construct(EmailTemplateRepository $emailTemplateRepository)
    {
        parent::__construct();

        $this->emailTemplateRepository = $emailTemplateRepository;

        view()->share('type', 'email_template');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = trans('email_template.email_templates');
        return View('user.email_template.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = trans('email_template.new');
        return view('user.email_template.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmailTemplateRequest $request)
    {
        $this->emailTemplateRepository->create($request->all());

        return redirect("email_template");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(EmailTemplate $emailTemplate)
    {
        $title = trans('email_template.edit');
        return view('user.email_template.edit', compact('title', 'emailTemplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(EmailTemplateRequest $request, EmailTemplate $emailTemplate)
    {
        $emailTemplate->update($request->all());

        return redirect("email_template");
    }

    public function show(EmailTemplate $emailTemplate)
    {
        $action = "show";
        $title = trans('email_template.show');
        return view('user.email_template.show', compact('title', 'emailTemplate', 'action'));
    }

    public function delete(EmailTemplate $emailTemplate)
    {
        $title = trans('email_template.delete');
        return view('user.email_template.delete', compact('title', 'emailTemplate'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplate $emailTemplate)
    {
        $emailTemplate->delete();
        return redirect('email_template');
    }

    /**
     * Get ajax datatables data
     *
     */
    public function data()
    {
        $email_templates = $this->emailTemplateRepository->getAll()
            ->get()
            ->map(function ($email_template) {
                return [
                    'id' => $email_template->id,
                    'title' => $email_template->title,
                ];
            });

        return Datatables::of($email_templates)
            ->add_column('actions', '<a href="{{ url(\'email_template/\' . $id . \'/edit\' ) }}" title="{{ trans(\'table.edit\') }}" >
                                            <i class="fa fa-fw fa-pencil text-warning"></i>  </a>
                                     <a href="{{ url(\'email_template/\' . $id . \'/show\' ) }}" title="{{ trans(\'table.details\') }}">
                                            <i class="fa fa-fw fa-eye text-primary"></i> </a>
                                     <a href="{{ url(\'email_template/\' . $id . \'/delete\' ) }}" title="{{ trans(\'table.delete\') }}">
                                            <i class="fa fa-fw fa-times text-danger"></i></a>')
            ->remove_column('id')
            ->make();
    }

    public function ajaxGetTemplate(EmailTemplate $emailTemplate)
    {
        return EmailTemplate::find($emailTemplate->id)->toArray();
    }
}
