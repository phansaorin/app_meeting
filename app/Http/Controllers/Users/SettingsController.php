<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\UserController;
use App\Http\Requests\SettingRequest;
use App\Models\Option;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Str;
use Efriandika\LaravelSettings\Facades\Settings;

class SettingsController extends UserController
{
    public function __construct()
    {
        parent::__construct();
        view()->share('type', 'setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = trans('settings.settings');
        $max_upload_file_size = array(
            '1000' => '1MB',
            '2000' => '2MB',
            '3000' => '3MB',
            '4000' => '4MB',
            '5000' => '5MB',
            '6000' => '6MB',
            '7000' => '7MB',
            '8000' => '8MB',
            '9000' => '9MB',
            '10000' => '10MB',
        );
        $options = Option::all()->flatten()->groupBy('category')->map(function ($grp) {
            return $grp->lists('value', 'title');
        });

        $opts = Option::all()->flatten()->groupBy('category')->map(function ($grp) {
            return $grp->map(function ($opt) {
                return [
                    'text' => $opt->value,
                    'id' => $opt->title
                ];
            })->values();
        });

        return view('user.setting.index', compact('title', 'max_upload_file_size', 'options', 'opts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SettingRequest|Request $request
     * @param Setting $setting
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(SettingRequest $request)
    {
        if ($request->hasFile('site_logo_file') != "") {
            $file = $request->file('site_logo_file');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = Str::slug(substr($filename, 0, strrpos($filename, "."))) . '_' . time() . '.' . $extension;

            $destinationPath = public_path() . '/uploads/site/';
            $file->move($destinationPath, $picture);
            $request->merge(['site_logo' => $picture]);
        }

        Settings::set('modules', []);
        foreach ($request->except('_token', 'site_logo_file') as $key => $value) {
            Settings::set($key, $value);
        }

        return redirect()->to('/setting');
    }
}
