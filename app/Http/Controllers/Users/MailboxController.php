<?php

namespace App\Http\Controllers\Users;

use App\Events\Email\EmailCreated;
use App\Http\Controllers\UserController;
use App\Http\Requests\MailboxRequest;
use App\Http\Requests\SupportRequest;
use App\Models\Company;
use App\Models\Email;
use App\Models\EmailTemplate;
use App\Models\User;
use App\Repositories\CompanyRepository;
use App\Repositories\ContractRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Efriandika\LaravelSettings\Facades\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Sentinel;
use App\Http\Requests;

class MailboxController extends UserController
{

    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ContractRepository
     */
    private $contractRepository;

    /**
     * @param CompanyRepository $companyRepository
     * @param UserRepository $userRepository
     * @param ContractRepository $contractRepository
     * @internal param CompanyRepository $
     */
    public function __construct(CompanyRepository $companyRepository,
                                UserRepository $userRepository,
                                ContractRepository $contractRepository)
    {
        parent::__construct();

        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
        $this->contractRepository = $contractRepository;

        view()->share('type', 'mailbox');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email_list = Email::where('to', $this->user->id)->where('delete_receiver','0')->orderBy('id','desc')->get();
        $sent_email_list = Email::where('from', $this->user->id)->where('delete_sender','0')->orderBy('id','desc')->get();
        $title = trans('mailbox.mailbox');

        $customers = ['' => trans('dashboard.select_company')] + $this->companyRepository->getAll()->orderBy("name", "asc")->lists('name', 'id')->toArray();

        $staffs = $this->userRepository->getAll()->where('id','<>',$this->user->id)
            ->get()->filter(function ($user) {
                return ($user->inRole('staff')||$user->inRole('admin'));
            })
            ->lists('full_name', 'id')->toArray();

        $users_active_list = $this->userRepository->getAll()
            ->where('id','<>',$this->user->id)->get()
            ->filter(function ($user) {
            return ($user->inRole('staff') || $user->inRole('admin') || $user->inRole('customer'));
        })
            ->map(function ($user) {
                return [
                    'full_name' => $user->full_name,
                    'user_avatar' => $user->user_avatar,
                    'active' => (isset($user->last_login) && $user->last_login >= Carbon::now()->subMinutes('15')->toDateTimeString()) ? '1' : '0',
                ];
            });

        $email_templates = ["0" => trans('mailbox.etemplate_select')] + EmailTemplate::lists('title', 'id')->toArray();

        return view('user.mailbox.index', compact('title', 'email_list', 'sent_email_list', 'staffs','customers',
            'email_templates', 'users_active_list'));
    }

    function sendEmail(MailboxRequest $request)
    {
        $message_return = '<div class="alert alert-danger">' . trans('mailbox.danger') . '</div>';
        if (!empty($request->to_email_id)) {
            foreach ($request->to_email_id as $item) {
                if ($item != "0" && $item != "") {
                    $email = new Email($request->except('to_email_id', 'assign_customer_id', 'etemplate_id'));
                    $email->to = $item;
                    $email->from = Sentinel::getUser()->id;
                    $email->save();

                    event(new EmailCreated($email));

                    $user = User::find($item);

                    if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
                        Mail::send('emails.contact', array('user' => $user->first_name . ' ' . $user->last_name, 'bodyMessage' => $request->message),
                            function ($m)
                            use ($user, $request) {
                                $m->from(Settings::get('site_email'), Settings::get('site_name'));
                                $m->to($user->email)->subject($request->subject);
                            });
                    }

                    $message_return = '<div class="alert alert-success">' . trans('mailbox.success') . '</div>';
                }

            }
        }
        if (isset($request->assign_customer_id) && $request->assign_customer_id != "0" &&
            $request->assign_customer_id != ""
        ) {
            $email = new Email($request->except('to_email_id', 'etemplate_id'));
            $email->from = Sentinel::getUser()->id;
            $email->save();

            $company = Company::find($request->assign_customer_id);


            if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
                Mail::send('emails.contact', array('user' => $company->name, 'bodyMessage' => $request->message),
                    function ($m)
                    use ($company, $request) {
                        $m->from(Settings::get('site_email'), Settings::get('site_name'));
                        $m->to($company->email)->subject($request->subject);
                    });
            }
            $message_return = '<div class="alert alert-success">' . trans('mailbox.success') . '</div>';
        }
        echo $message_return;

    }

    function deleteMail(Email $mail)
    {
        if($mail->to == $this->user->id){
            $mail->delete_receiver= 1;
        }
        else{
            $mail->delete_sender= 1;
        }
        $mail->save();
    }

    function readMail(Email $mail)
    {
        $mail->read = 1;
        $mail->save();
    }

    public function support()
    {
        $email_list = Email::orderBy("id", "desc")->where('assign_customer_id', 0)->where('to', Sentinel::getUser()->id)->get();
        $sent_email_list = Email::orderBy("id", "desc")->where('assign_customer_id', 0)->where('from', Sentinel::getUser()->id)->get();

        $title = trans('mailbox.support');

        if (Sentinel::inRole('admin')) {
            return view('user.mailbox.support', compact('title', 'email_list', 'sent_email_list'));
        } else {
            return view('user.mailbox.support2', compact('title', 'email_list', 'sent_email_list'));
        }
    }


    function sendSupport(SupportRequest $request)
    {
        $role = Sentinel::findRoleBySlug('admin');
        $admin = $role->users()->first();
        foreach ($request->to as $to) {
            $user = User::find($to);

            $email = new Email($request->except('to', 'etemplate_id'));
            $email->from = $admin->id;
            $email->to = $to;
            $email->save();


            if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
                Mail::send('emails.support', array('user' => $admin->first_name . ' ' . $admin->last_name, 'bodyMessage' => $request->message),
                    function ($m)
                    use ($user) {
                        $m->from(Settings::get('site_email'), Settings::get('site_name'));
                        $m->to($user->email)->subject('Contact message');
                    });
            }
        }
        echo '<div class="alert alert-success">' . trans('mailbox.success') . '</div>';
    }

    function sendSupportReplay(SupportRequest $request)
    {
        $role = Sentinel::findRoleBySlug('admin');
        $admin = $role->users()->first();
        $user = User::find($request->to);

        $email = new Email($request->except('to', 'etemplate_id'));
        $email->from = $admin->id;
        $email->to = $request->to;
        $email->save();


        if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
            Mail::send('emails.support', array('user' => $admin->first_name . ' ' . $admin->last_name, 'bodyMessage' => $request->message),
                function ($m)
                use ($user) {
                    $m->from(Settings::get('site_email'), Settings::get('site_name'));
                    $m->to($user->email)->subject('Contact message-replay');
                });
        }
        echo '<div class="alert alert-success">' . trans('mailbox.success') . '</div>';
    }


    public function getAllData()
    {
        $total = $this->user->emails()->whereRead(false)->count();
        $emails = $this->user->emails()->with('sender')->latest()->take(5)->whereRead(false)->get();

        return response()->json(compact('total', 'emails'), 200);
    }

    public function postRead(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $model = Email::find($request->get('id'));
        $model->read = true;
        $model->save();

        return response()->json(['message' => trans('mailbox.update_status')], 200);
    }

}
