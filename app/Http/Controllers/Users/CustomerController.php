<?php

namespace App\Http\Controllers\Users;

use App\Helpers\Thumbnail;
use App\Http\Controllers\UserController;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use App\Models\Option;
use App\Models\User;
use App\Repositories\CompanyRepository;
use App\Repositories\ExcelRepository;
use App\Repositories\SalesTeamRepository;
use App\Repositories\UserRepository;
use Settings;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use yajra\Datatables\Datatables;

class CustomerController extends UserController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var SalesTeamRepository
     */
    private $salesTeamRepository;
    /**
     * @var ExcelRepository
     */
    private $excelRepository;

    /**
     * CustomerController constructor.
     * @param UserRepository      $userRepository
     * @param CompanyRepository   $companyRepository
     * @param SalesTeamRepository $salesTeamRepository
     * @param ExcelRepository     $excelRepository
     */
    public function __construct(UserRepository $userRepository,
                                CompanyRepository $companyRepository,
                                SalesTeamRepository $salesTeamRepository,
                                ExcelRepository $excelRepository)
    {
        parent::__construct();

        $this->middleware('authorized:contacts.read', ['only' => ['index', 'data']]);
        $this->middleware('authorized:contacts.write', ['only' => ['create', 'store', 'update', 'edit']]);
        $this->middleware('authorized:contacts.delete', ['only' => ['delete']]);

        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
        $this->salesTeamRepository = $salesTeamRepository;
        $this->excelRepository = $excelRepository;

        view()->share('type', 'customer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = trans('customer.customers');

        $companies = $this->companyRepository->getAll()
            ->orderBy("name", "asc")
            ->get()
            ->map(function ($c) {
                return [
                    'text' => $c->name,
                    'id'   => $c->id,
                ];
            })->values();

        $customers = $this->userRepository->getAll()
            ->with('customer.company')
            ->get()
            ->filter(function ($user) {
                return $user->inRole('customer', 'companies');
            });

        return view('user.customer.index', compact('title', 'companies', 'customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = trans('customer.new');

        $this->generateParams();

        return view('user.customer.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $user = Sentinel::registerAndActivate($request->only('first_name', 'last_name', 'phone_number', 'email', 'password'));

        $role = Sentinel::findRoleBySlug('customer');
        $role->users()->attach($user);

        $user = User::find($user->id);

        if ($request->hasFile('user_avatar_file')) {
            $file = $request->file('user_avatar_file');
            $file = $this->userRepository->uploadAvatar($file);

            $request->merge([
                'user_avatar' => $file->getFileInfo()->getFilename(),
            ]);

            $this->generateThumbnail($file);

            $user->user_avatar = $request->user_avatar;
        }
        $user->password = bcrypt($request->password);
        $user->save();

        $customer = new Customer($request->except('first_name', 'last_name', 'phone_number', 'email', 'password',
            'user_avatar', 'password_confirmation', 'user_avatar_file'));
        $customer->user_id = $user->id;
        $customer->belong_user_id = Sentinel::getUser()->id;
        $customer->save();

        $subject = 'Customer login details';

        $currentUser = Sentinel::getUser();
        $currentUser->users()->save($user);

        if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
            Mail::send('emails.new_customer', array('email' => $request->email, 'password' => $request->password), function ($m) use ($request, $subject) {
                $m->from(Settings::get('site_email'), Settings::get('site_name'));
                $m->to($request->email, $request->first_name . $request->last_name);
                $m->subject($subject);
            });
        }

        return redirect("customer");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $title = trans('customer.edit');

        $this->generateParams();

        return view('user.customer.edit', compact('title', 'user'));
    }

    public function ajaxUpdate(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name'             => 'required|alpha',
            'last_name'              => 'required|alpha',
            'email'                  => 'required|email||unique:users,email,' . $user->id,
            'customer.mobile'        => 'numeric',
            'customer.website'       => 'url',
            'customer.sales_team_id' => 'required',
        ]);

        $user->update($request->only('first_name', 'last_name', 'email'));

        $customerData = $request->get('customer');

        $user->customer->fill($customerData);
        $user->customer->company()->associate($customerData['company_id']);
        $user->customer->salesteam()->associate($customerData['sales_team_id']);
        $user->customer->save();

        $user->load('customer.company', 'customer.salesteam');

        return response()->json(['message' => 'Updated successfully!', 'data' => compact('user')], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  User                     $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($request->hasFile('user_avatar')) {
            $destinationPath = public_path() . '/uploads/avatar/';
            $file_temp = $request->file('user_avatar');
            $extension = $file_temp->getClientOriginalExtension() ?: 'png';
            $safeName = str_random(10) . '.' . $extension;
            $file_temp->move($destinationPath, $safeName);
            Thumbnail::generate_image_thumbnail($destinationPath . $safeName, $destinationPath . 'thumb_' . $safeName);
            $user->user_avatar = $safeName;
        }
        if ($request->password != "") {
            $user->password = bcrypt($request->password);
        }
        $user->update($request->only('first_name', 'last_name', 'phone_number'));

        $customer = Customer::where('user_id', $user->id)->first();
        $customer->update($request->except('first_name', 'last_name', 'phone_number', 'email', 'password', 'user_avatar', 'password_confirmation'));

        return redirect("customer");
    }


    public function delete(User $user)
    {
        $title = trans('customer.delete');

        return view('user.customer.delete', compact('title', 'user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect('customer');
    }

    public function data()
    {
        $customers = $this->userRepository->getAll()
            ->with('customer.company', 'customer.salesteam')
            ->get()
            ->filter(function ($user) {
                return $user->inRole('customer');
            })->values();

        $companies = $this->companyRepository->getAll()
            ->orderBy("name", "asc")
            ->get()
            ->map(function ($company) {
                return [
                    'text' => $company->name,
                    'id'   => $company->id,
                ];
            })->values();

        $salesTeams = $this->salesTeamRepository->getAll()
            ->orderBy("id", "asc")
            ->get()
            ->map(function ($salesTeam) {
                return [
                    'text' => $salesTeam->salesteam,
                    'id'   => $salesTeam->id,
                ];
            })->values();

        $titles = Option::where('category', 'titles')
            ->get()
            ->map(function ($title) {
                return [
                    'text' => $title->title,
                    'id'   => $title->value,
                ];
            })->values();
        $can_write = (Sentinel::getUser()->hasAccess(['contacts.write']) || Sentinel::inRole('admin'))?true:false;
        $can_delete = (Sentinel::getUser()->hasAccess(['contacts.delete']) || Sentinel::inRole('admin'))?true:false;

        return response()->json(compact('customers', 'companies', 'salesTeams', 'titles','can_write','can_delete'), 200);

        $customers = $this->userRepository->getAll()
            ->get()
            ->filter(function ($user) {
                return $user->inRole('customer');
            })
            ->map(function ($user) {
                return [
                    'id'         => $user->id,
                    'full_namae' => $user->full_name,
                    'email'      => $user->email,
                    'created_at' => $user->created_at->format('M j Y'),
                ];
            });

        return Datatables::of($customers)
            ->add_column('actions', '@if(Sentinel::getUser()->hasAccess([\'contacts.write\']) || Sentinel::inRole(\'admin\'))
                                    <a href="{{ url(\'customer/\' . $id . \'/edit\' ) }}"  title="{{ trans(\'table.edit\') }}">
                                            <i class="fa fa-fw fa-pencil text-warning "></i> </a>
                                    @endif
                                     @if(Sentinel::getUser()->hasAccess([\'contacts.delete\']) || Sentinel::inRole(\'admin\'))
                                    <a href="{{ url(\'customer/\' . $id . \'/delete\' ) }}" title="{{ trans(\'table.delete\') }}">
                                            <i class="fa fa-fw fa-times text-danger"></i> </a>
                                    @endif')
            ->remove_column('id')
            ->make();
    }

    public function importExcelData(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls,csv|max:5000',
        ]);

        $reader = $this->excelRepository->load($request->file('file'));

        $users = $reader->all()->map(function ($row) {
            return [
                'email'      => $row->email,
                'password'   => $row->password,
                'first_name' => $row->first_name,
                'last_name'  => $row->last_name,
                'mobile'     => $row->mobile,
                'fax'        => $row->fax,
                'website'    => $row->website,
            ];
        });

        foreach ($users as $userData) {
            if (!$customer = \App\Models\User::whereEmail($userData['email'])->first()) {
                $customer = $this->userRepository->create($userData);

                $customer->customer()->create($userData);
                $this->userRepository->assignRole($customer, 'customer');
            }
        }

        return response()->json([], 200);
    }

    public function downloadExcelTemplate()
    {
        return response()->download(base_path('resources/excel-templates/contacts.xlsx'));
    }

    private function generateParams()
    {
        $salesteams = ['' => trans('dashboard.select_sales_team')] + $this->salesTeamRepository->getAll()->orderBy("id", "asc")->lists('salesteam', 'id')->toArray();
        $companies = ['' => trans('dashboard.select_company')] + $this->companyRepository->getAll()->orderBy("name", "asc")->lists('name', 'id')->toArray();
        $titles = Option::where('category', 'titles')->lists('title', 'value');

        view()->share('salesteams', $salesteams);
        view()->share('companies', $companies);
        view()->share('titles', $titles);
    }


    public function getImport()
    {
        $title = trans('customer.customers');

        return view('user.customer.import', compact('title'));
    }

    public function postImport(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx,xls,csv|max:5000',
        ]);

        $reader = $this->excelRepository->load($request->file('file'));

        $customers = $reader->all()->map(function ($row) {
            return [
                'first_name'            => $row->first_name,
                'last_name'             => $row->last_name,
                'email'                 => $row->email,
                'password'              => $row->password,
                'password_confirmation' => $row->password,
                'mobile'                => $row->mobile,
                'webstie'               => $row->website,
            ];
        });

        $companies = $this->companyRepository->getAll()->get()->map(function ($company) {
            return [
                'text' => $company->name,
                'id'   => $company->id,
            ];
        })->values();

        return response()->json(compact('customers', 'companies'), 200);
    }

    public function postAjaxStore(CustomerRequest $request)
    {
        $this->userRepository->create($request->except('created', 'errors', 'selected'));

        return response()->json([], 200);
    }

    /**
     * @param $file
     */
    private function generateThumbnail($file)
    {
        Thumbnail::generate_image_thumbnail(public_path() . '/uploads/avatar/' . $file->getFileInfo()->getFilename(),
            public_path() . '/uploads/avatar/' . 'thumb_' . $file->getFileInfo()->getFilename());
    }
}
