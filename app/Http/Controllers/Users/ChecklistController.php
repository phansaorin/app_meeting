<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\UserController;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Checklist;
use yajra\Datatables\Datatables;
use Sentinel;

class ChecklistController extends UserController
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        view()->share('type', 'checklist');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = trans('checklist.title');
        return view('user.checklist.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = trans('checklist.new');
        return view('user.checklist.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $checklist = new Checklist;
       $checklist->insert($request->all());
       return redirect("checklist");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = trans('checklist.new');
        $checklist = Checklist::find($id);
        return view('user.checklist.create', compact('title','checklist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request = $request->all();
      $data = [
        'title' => $request['title'],
        'bussines_nature' => $request['bussines_nature'],
        'description' => $request['description']
      ];

      Checklist::do_update($data, $id);
      return redirect('checklist');
    }

    public function delete($id)
    {
      $action = '';
      $checklist = Checklist::get_info($id);
      $title = trans('checklist.delete');
      return view('user.checklist.delete', compact('title', 'checklist','action'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Checklist::do_delete($id);
       return redirect('checklist');
    }

    public function data()
   {
       // $checklist = new Checklist();
       $checklist = Checklist::get_all()
           ->map(function ($checklist) {
               return [
                   'id' => $checklist->id,
                   'title' => $checklist->title,
                   'bussines_nature' => $checklist->bussines_nature,
                   'description' => $checklist->description,
               ];
           });
        return Datatables::of($checklist)
           ->add_column('actions', '<a href="{{ url(\'checklist/\' . $id . \'/edit\' ) }}"  title="{{ trans(\'table.edit\') }}">
                           <i class="fa fa-fw fa-pencil text-warning"></i> </a>
                           <a href="{{ url(\'checklist/\' . $id . \'/delete\' ) }}"  title="{{ trans(\'table.delete\') }}">
                           <i class="fa fa-fw fa-times text-danger"></i> </a>
                    ')
           ->remove_column('id')
           ->make();
   }
}
