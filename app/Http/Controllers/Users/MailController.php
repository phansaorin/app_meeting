<?php namespace App\Http\Controllers\Users;


use App\Events\Email\EmailCreated;
use App\Http\Controllers\UserController;
use App\Models\Email;
use App\Models\EmailTemplate;
use App\Models\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Efriandika\LaravelSettings\Facades\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends UserController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * MailController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    public function getReceived(Request $request)
    {
        $received = Email::where('to', $this->user->id)
            ->where('delete_receiver', '0')
            ->where('subject', 'like', '%' . $request->get('query', '') . '%')
            ->with('sender')
            ->orderBy('id', 'desc')->get();

        return response()->json(compact('received'), 200);
    }

    public function getData()
    {
        $email_count = Email::where('to', $this->user->id)->where('delete_receiver', '0')->whereRead(0)->count();
        $sent_email_count = Email::where('from', $this->user->id)->where('delete_sender', '0')->count();


        $staff = $this->userRepository->getAll()
            ->where('id', '<>', $this->user->id)
            ->get()->filter(function ($user) {
                return ($user->inRole('staff') || $user->inRole('admin'));
            })->map(function ($user) {
                return [
                    'id'   => $user->id,
                    'text' => $user->full_name,
                ];
            })->values();

        $email_templates = EmailTemplate::all()->map(function ($t) {
            return [
                'id'   => $t->id,
                'text' => $t->title,
            ];
        })->values();


        $users = $this->userRepository->getAll()
            ->where('id', '<>', $this->user->id)->get()
            ->filter(function ($user) {
                return ($user->inRole('staff') || $user->inRole('admin') || $user->inRole('customer'));
            })
            ->map(function ($user) {
                return [
                    'full_name'   => $user->full_name,
                    'user_avatar' => $user->user_avatar,
                    'active'      => (isset($user->last_login) && $user->last_login >= Carbon::now()->subMinutes('15')->toDateTimeString()) ? '1' : '0',
                ];
            });

        return response()->json(compact('staff', 'email_templates', 'email_count', 'sent_email_count', 'users'), 200);
    }

    public function getSent()
    {
        $sent = Email::where('from', $this->user->id)
            ->where('delete_sender', '0')
            ->with('receiver')
            ->orderBy('id', 'desc')->get();

        return response()->json(compact('sent'), 200);
    }

    public function getMail($id)
    {
        $email = Email::with('sender')->find($id);

        return response()->json(compact('email'), 200);
    }

    public function getMailTemplate($id)
    {
        $template = EmailTemplate::find($id);

        return response()->json(compact('template'), 200);
    }

    public function postSend(Request $request)
    {
        foreach ($request->recipients as $item) {
            if ($item != "0" && $item != "") {
                $email = new Email($request->except('recipients', 'emailTemplate'));
                $email->to = $item;
                $email->from = \Sentinel::getUser()->id;
                $email->save();

                event(new EmailCreated($email));

                $user = User::find($item);


                if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
                    Mail::send('emails.contact', array('user' => $user->full_name, 'bodyMessage' => $request->message),
                        function ($m)
                        use ($user, $request) {
                            $m->from(Settings::get('site_email'), Settings::get('site_name'));
                            $m->to($user->email)->subject($request->subject);
                        });
                }

                $message_return = '<div class="alert alert-success">' . trans('mailbox.success') . '</div>';
            }
        }

        return response()->json(['message' => 'Message sent successfully!'], 200);

    }

    public function postMarkAsRead(Request $request)
    {
        if ($ids = $request->get('ids')) {
            if (is_array($ids)) {
                $emails = Email::whereIn('id', $ids)->get();
                foreach ($emails as $email) {
                    $email->read = true;
                    $email->save();
                }
            } else {
                $email = Email::find($ids);
                $email->read = true;
                $email->save();
            }
        }
    }

    public function postDelete(Request $request)
    {
        if ($ids = $request->get('ids')) {
            if (is_array($ids)) {
                $emails = Email::whereIn('id', $ids)->get();
                foreach ($emails as $email) {
                    $email->delete();
                }
            } else {
                $email = Email::find($ids);
                $email->delete();
            }
        }
    }

    public function postReply($id, Request $request)
    {
        $orgMail = Email::find($id);

        $request->merge([
            'subject' => 'Re: ' . $orgMail->subject,
        ]);

        $email = new Email($request->all());
        $email->to = $orgMail->from;
        $email->from = \Sentinel::getUser()->id;
        $email->save();

        event(new EmailCreated($email));

        $user = $orgMail->sender;


        if (!filter_var(Settings::get('site_email'), FILTER_VALIDATE_EMAIL) === false) {
            Mail::send('emails.contact', array('user' => $user->full_name, 'bodyMessage' => $request->message),
                function ($m)
                use ($user, $request) {
                    $m->from(Settings::get('site_email'), Settings::get('site_name'));
                    $m->to($user->email)->subject($request->subject);
                });
        }

    }


}