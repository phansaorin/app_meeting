<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\User;
use Efriandika\LaravelSettings\Facades\Settings;

class CustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name' => 'required|min:3|max:50|alpha',
                    'last_name' => 'required|min:3|max:50|alpha',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|min:6|confirmed',
                    'phone_number' => 'required|numeric|min:5',
                    'sales_team_id' => 'required',
                    'website' => 'url',
                    'mobile' => 'numeric|min:5',
                    'fax' => 'numeric|min:5',
                    'user_avatar' => 'mimes:'.Settings::get('allowed_extensions').'|image|max:'.Settings::get('max_upload_file_size'),
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                if (preg_match("/\/(\d+)$/", $this->url(), $mt))
                    $user = User::find($mt[1]);
                return [
                    'first_name' => 'required|min:3|max:50|alpha',
                    'last_name' => 'required|min:3|max:50|alpha',
                    'email' => 'required|email|unique:users,email,'.$user->id,
                    'password' => 'min:6|confirmed',
                    'website' => 'url',
                    'phone_number' => 'required|numeric|min:5',
                    'sales_team_id' => 'required',
                    'mobile' => 'numeric|min:5',
                    'fax' => 'numeric|min:5',
                    'user_avatar' => 'mimes:'.Settings::get('allowed_extensions').'|image|max:'.Settings::get('max_upload_file_size'),
                ];
            }
            default:break;
        }

        return [

        ];
    }
}
