<?php

namespace App\Http\Requests;

use App\Models\Company;
use Efriandika\LaravelSettings\Facades\Settings;

class CompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->segment(2) != "") {
            $company = Company::find($this->segment(2));
        }

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required|min:3|max:50',
                    'email' => 'required|email',
                    'address' => 'required',
                    'sales_team_id' => 'required',
                    'country_id' => 'required',
                    'main_contact_person' => 'required',
                    'phone' => 'required|numeric|min:5',
                    'website' => 'required|url',
                    'mobile' => 'numeric|min:5',
                    'fax' => 'numeric|min:5',
                    'company_avatar' => 'mimes:'.Settings::get('allowed_extensions').'|image|max:'.Settings::get('max_upload_file_size'),
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|min:3|max:50',
                    'phone' => 'required|numeric|min:5',
                    'mobile' => 'numeric|min:5',
                    'fax' => 'numeric|min:5',
                    'address' => 'required',
                    'website' => 'required|url',
                    'country_id' => 'required',
                    'main_contact_person' => 'required',
                    'sales_team_id' => 'required',
                    'email' => 'required|email|unique:companies,email,' . $company->id,
                    'company_avatar' =>'mimes:'.Settings::get('allowed_extensions').'|image|max:'.Settings::get('max_upload_file_size'),
                ];
            }
            default:
                break;
        }

        return [

        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        //$this->merge(['ip_address' => $this->ip()]);
        //$this->merge(['register_time' => strtotime(date('d F Y g:i a'))]);
        return parent::getValidatorInstance();
    }
}
