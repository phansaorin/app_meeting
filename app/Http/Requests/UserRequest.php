<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\User;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'first_name' => 'required|min:3|max:50|alpha',
                    'last_name' => 'required|min:3|max:50|alpha',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|min:6|confirmed',
                    'phone_number' => 'numeric',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                if (preg_match("/\/(\d+)$/", $this->url(), $mt))
                    $user = User::find($mt[1]);

                return [
                    'first_name' => 'required|min:3|max:50|alpha',
                    'last_name' => 'required|min:3|max:50|alpha',
                    'email' => 'required|email|unique:users,email,' . $user->id,
                    'password' => 'min:6|confirmed',
                    'phone_number' => 'numeric',
                ];
            }
            default:
                break;
        }

        return [

        ];
    }
}
