<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LeadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'opportunity' => 'required',
            'email' => 'required|email',
            'customer_id' => 'required',
            'sales_person_id' => 'required',
            'sales_team_id' => 'required',
            'company_name' => 'required',
            'contact_name' => 'required',
            'function' => 'required',
            'country_id' => 'required',
            'phone' => 'numeric|required|min:5',
            'mobile' => 'numeric|min:5',
            'fax' => 'numeric|min:5'
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
       // $this->merge(['ip_address' => $this->ip()]);
        $this->merge(['tags' => implode(',', $this->get('tags', []))]);
        return parent::getValidatorInstance();
    }




}
