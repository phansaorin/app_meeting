<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Checklist extends Model
{
    use SoftDeletes,RevisionableTrait;

    protected $guarded = array('id');
    protected $table = 'checklists';
    // protected $dates = ['deleted_at'];
	public function insert(array $data)
	{
		return Checklist::create($data);
	}  

	public static function get_all()
	{
		return Checklist::all();
	}

	public static function do_update(array $data, $id=false)
	{
	   return DB::table('checklists')->where('id', $id)->update($data);
	}

	public static function get_info($id)
	{
	   $checklists = DB::table('checklists')->select('title', 'id')->where('id', '=', $id)->get();
	   return $checklists[0];
	}


	public static function do_delete($id)
   {
       return DB::table('checklists')->where('id', $id)->delete();
   }
}
