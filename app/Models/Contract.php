<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Contract extends Model
{
    use SoftDeletes,RevisionableTrait;

    protected $dates = ['deleted_at'];
    protected $guarded  = array('id');
    protected $table = 'contracts';

    public function setStartDateAttribute($start_date)
    {
        if ($start_date) {
            $this->attributes['start_date'] = date('Y-m-d',strtotime($start_date));
        } else {
            $this->attributes['start_date'] = '';
        }
    }

    public function getStartDateAttribute($start_date)
    {
        if ($start_date == "0000-00-00" || $start_date == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($start_date));
        }
    }

    public function setEndDateAttribute($end_date)
    {
        if ($end_date) {
            $this->attributes['end_date'] = date('Y-m-d',strtotime($end_date));
        } else {
            $this->attributes['end_date'] = '';
        }
    }

    public function getEndDateAttribute($end_date)
    {
        if ($end_date == "0000-00-00" || $end_date == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($end_date));
        }
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }
}
