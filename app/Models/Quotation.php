<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Quotation extends Model
{
    use SoftDeletes,RevisionableTrait;

    protected $dates = ['deleted_at'];
    protected $guarded = array('id');
    protected $table = 'quotations';

    public function products()
    {
        return $this->hasMany(QuotationProduct::class, 'quotation_id');
    }

    public function setDateAttribute($date)
    {
        if ($date) {
            $this->attributes['date'] = date('Y-m-d',strtotime($date));
        } else {
            $this->attributes['date'] = '';
        }
    }

    public function getDateAttribute($date)
    {
        if ($date == "0000-00-00" || $date == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($date));
        }
    }
    public function setExpDateAttribute($exp_date)
    {
        if ($exp_date) {
            $this->attributes['exp_date'] = date('Y-m-d',strtotime($exp_date));
        } else {
            $this->attributes['exp_date'] = '';
        }
    }

    public function getExpDateAttribute($exp_date)
    {
        if ($exp_date == "0000-00-00" || $exp_date == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($exp_date));
        }
    }


    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function salesPerson()
    {
        return $this->belongsTo(User::class, 'sales_person_id');
    }

    public function salesTeam()
    {
        return $this->belongsTo(Salesteam::class, 'sales_team_id');
    }
}
