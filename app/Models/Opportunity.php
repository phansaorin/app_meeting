<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Opportunity extends Model
{
    use SoftDeletes, CallableTrait, MeetableTrait, RevisionableTrait;

    protected $dates = ['deleted_at'];
    protected $guarded = array('id');
    protected $table = 'opportunities';

    public function setNextActionAttribute($next_action)
    {
        if ($next_action) {
            $this->attributes['next_action'] = date('Y-m-d',strtotime($next_action));
        } else {
            $this->attributes['next_action'] = '';
        }
    }

    public function getNextActionAttribute($next_action)
    {
        if ($next_action == "0000-00-00" || $next_action == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($next_action));
        }
    }

    public function setExpectedClosingAttribute($expected_closing)
    {
        if ($expected_closing) {
            $this->attributes['expected_closing'] = date('Y-m-d',strtotime($expected_closing));
        } else {
            $this->attributes['expected_closing'] = '';
        }
    }

    public function getExpectedClosingAttribute($expected_closing)
    {
        if ($expected_closing == "0000-00-00" || $expected_closing == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($expected_closing));
        }
    }
    public function customer()
    {
        return $this->belongsTo(Company::class, 'customer_id');
    }
    public function salesTeam()
    {
        return $this->belongsTo(Salesteam::class, 'sales_team_id');
    }

    public function salesPerson()
    {
        return $this->belongsTo(User::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
