<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Meeting extends Model
{
    use SoftDeletes,RevisionableTrait;

    protected $dates = ['deleted_at'];
    protected $guarded = array('id');
    protected $table = 'meetings';

    public function setStartingDateAttribute($starting_date)
    {
        if ($starting_date) {
            $this->attributes['starting_date'] = date('Y-m-d H:i',strtotime($starting_date));
        } else {
            $this->attributes['starting_date'] = '';
        }
    }

    public function getStartingDateAttribute($starting_date)
    {
        if ($starting_date == "0000-00-00 00:00" || $starting_date == "") {
            return "";
        } else {
            return date('d.m.Y. H:i', strtotime($starting_date));
        }
    }

    public function setEndingDateAttribute($ending_date)
    {
        if ($ending_date) {
            $this->attributes['ending_date'] = date('Y-m-d H:i',strtotime($ending_date));
        } else {
            $this->attributes['ending_date'] = '';
        }
    }

    public function getEndingDateAttribute($ending_date)
    {
        if ($ending_date == "0000-00-00 00:00" || $ending_date == "") {
            return "";
        } else {
            return date('d.m.Y. H:i', strtotime($ending_date));
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function responsible()
    {
        return $this->belongsTo(User::class, 'responsible_id');
    }
}
