<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;


class Call extends Model
{
    use SoftDeletes,RevisionableTrait;

    protected $guarded = array('id');
    protected $table = 'calls';
    protected $dates = ['deleted_at'];

    public function setDateAttribute($date)
    {
        if ($date) {
            $this->attributes['date'] = date('Y-m-d',strtotime($date));
        } else {
            $this->attributes['date'] = '';
        }
    }

    public function getDateAttribute($date)
    {
        if ($date == "0000-00-00" || $date == "") {
            return "";
        } else {
            return date('d.m.Y.', strtotime($date));
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function responsible()
    {
        return $this->belongsTo(User::class, 'resp_staff_id');
    }

    public function opportunity()
    {
        return $this->morphedByMany(Opportunity::class, 'callables');
    }

    public function lead()
    {
        return $this->morphedByMany(Lead::class, 'callables');
    }

}
